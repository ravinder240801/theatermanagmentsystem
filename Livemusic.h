#ifndef _LIVEMUSIC_H_
#define _LIVEMUSIC_H_
/*
    Livemusic.h
    Author: M00804377
    Created: 25 Jan,2022
    Updated: 25 Jan,2022
*/
#include <iostream>
#include <string>
#include "Event.h"
using namespace std;

class Livemusic : public Event
{
public:
    string singer;
    string band;
    void set_music_data();
    string get_details_string();
    string get_file_string();
};
#endif // _LIVEMUSIC_H_