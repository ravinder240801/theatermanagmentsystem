/*
    Livemusic.cpp
    Author: M00804377
    Created: 25 Jan,2022
    Updated: 25 Jan,2022
*/
#include <iostream>
#include "LiveMusic.h"

using namespace std;

void Livemusic ::set_music_data()
{
    cout << "Enter name of singer: ";
    cin >> singer;

    cout << "Enter name of band: ";
    cin >> band;
}

string Livemusic::get_details_string()
{
    return "Name: " + name + ", Capacity: " + to_string(capacity) + ", Singer: " + singer + ", Band: " + band;
}

string Livemusic::get_file_string()
{
    return "m;" + name + ";" + to_string(capacity) + ";" + singer + ";" + band + ";\n";
}