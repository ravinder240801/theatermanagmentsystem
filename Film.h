#ifndef _FILM_H_
#define _FILM_H_
/*
    Film.h
    Author: M00804377
    Created: 25 Jan,2022
    Updated: 25 Jan,2022
*/
#include <iostream>
#include <string>
#include "Event.h"
using namespace std;

class Film : public Event
{
public:
    string hero;
    string dimension;

    void set_film_data();
    string get_details_string();
    string get_file_string();
};
#endif // _FILM_H_