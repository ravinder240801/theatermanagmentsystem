#ifndef _STANDUPCOMEDY_H_
#define _STANDUPCOMEDY_H_
/*
    Standupcomedy.h
    Author: M00804377
    Created: 25 Jan,2022
    Updated: 25 Jan,2022
*/
#include <iostream>
#include <string>
#include "Event.h"
using namespace std;

class Standupcomedy : public Event
{
public:
    string comedian;
    void set_standup_data();
    string get_details_string();
    string get_file_string();
};
#endif // _STANDUPCOMEDY_H_