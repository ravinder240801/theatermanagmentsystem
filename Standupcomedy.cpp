/*
    Standupcomedy.cpp
    Author: M00804377
    Created: 25 Jan,2022
    Updated: 25 Jan,2022
*/
#include <iostream>
#include "Standupcomedy.h"

using namespace std;

void Standupcomedy :: set_standup_data()
{
    cout << "Enter comedian name: ";
    cin >> comedian; 
}

string Standupcomedy::get_details_string()
{
    return "Name: " + name + ", Capacity: " + to_string(capacity) + ", Comedian: " + comedian;
}

string Standupcomedy::get_file_string()
{
    return "c;" + name + ";" + to_string(capacity) + ";" + comedian + ";\n";
}