/*
    Film.cpp
    Author: M00804377
    Created: 25 Jan,2022
    Updated: 25 Jan,2022
*/
#include <iostream>
#include "Film.h"

using namespace std;

void Film::set_film_data()
{
    cout << "Enter the hero name: ";
    cin >> hero;

    cout << "Is it 2D or 3D?";
    cin >> dimension;
}

string Film::get_details_string()
{
    return "Name: " + name + ", Capacity: " + to_string(capacity) + ", Hero: " + hero + ", Dimension: " + dimension;
}

string Film::get_file_string()
{
    return "f;" + name + ";" + to_string(capacity) + ";" + hero + ";" + dimension + ";\n";
}