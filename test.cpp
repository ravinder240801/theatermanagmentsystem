#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <iostream>
#include <vector>
#include <fstream>
#include <limits>
#include <sstream>
#include "Film.h"
#include "LiveMusic.h"
#include "StandUpComedy.h"

TEST_CASE("Test get_details_string Film")
{
    //Arrange
    Film f;
    f.name = "TestFilm";
    f.capacity = 100;
    f.hero = "TestHero";
    f.dimension = "2D";

    //Act
    string res = f.get_details_string();

    //Assert
    REQUIRE_THAT(res, Catch::Matchers::Contains("TestFilm"));
    REQUIRE_THAT(res, Catch::Matchers::Contains("TestHero"));
    REQUIRE_THAT(res, Catch::Matchers::Contains("100"));
    REQUIRE_THAT(res, Catch::Matchers::Contains("2D"));
}


TEST_CASE("Test get_details_string Stand Up Comedy")
{
    //Arrange
    Standupcomedy s;
    s.name = "TestComedyShow";
    s.capacity = 90;
    s.comedian = "TestComedian";

    //Act
    string res = s.get_details_string();

    //Assert
    REQUIRE_THAT(res, Catch::Matchers::Contains("TestComedyShow"));
    REQUIRE_THAT(res, Catch::Matchers::Contains("TestComedian"));
    REQUIRE_THAT(res, Catch::Matchers::Contains("90"));
}


TEST_CASE("Test get_details_string Live Music")
{
    //Arrange
    Livemusic l;
    l.name = "TestLiveMusic";
    l.capacity = 150;
    l.singer = "TestSinger";
    l.band = "TestBand";

    //Act
    string res = l.get_details_string();

    //Assert
    REQUIRE_THAT(res, Catch::Matchers::Contains("TestLiveMusic"));
    REQUIRE_THAT(res, Catch::Matchers::Contains("TestSinger"));
    REQUIRE_THAT(res, Catch::Matchers::Contains("150"));
    REQUIRE_THAT(res, Catch::Matchers::Contains("TestBand"));
}


TEST_CASE("Test get_file_string Film")
{
    //Arrange
    Film f;
    f.name = "TestFilm";
    f.capacity = 100;
    f.hero = "TestHero";
    f.dimension = "2D";

    //Act
    string res = f.get_file_string();

    //Assert
    REQUIRE_THAT(res, Catch::Matchers::Contains("TestFilm"));
    REQUIRE_THAT(res, Catch::Matchers::Contains("TestHero"));
    REQUIRE_THAT(res, Catch::Matchers::Contains("100"));
    REQUIRE_THAT(res, Catch::Matchers::Contains("2D"));
}


TEST_CASE("Test get_file_string Stand Up Comedy")
{
    //Arrange
    Standupcomedy s;
    s.name = "TestComedyShow";
    s.capacity = 90;
    s.comedian = "TestComedian";

    //Act
    string res = s.get_file_string();

    //Assert
    REQUIRE_THAT(res, Catch::Matchers::Contains("TestComedyShow"));
    REQUIRE_THAT(res, Catch::Matchers::Contains("TestComedian"));
    REQUIRE_THAT(res, Catch::Matchers::Contains("90"));
}


TEST_CASE("Test get_file_string Live Music")
{
    //Arrange
    Livemusic l;
    l.name = "TestLiveMusic";
    l.capacity = 150;
    l.singer = "TestSinger";
    l.band = "TestBand";

    //Act
    string res = l.get_file_string();

    //Assert
    REQUIRE_THAT(res, Catch::Matchers::Contains("TestLiveMusic"));
    REQUIRE_THAT(res, Catch::Matchers::Contains("TestSinger"));
    REQUIRE_THAT(res, Catch::Matchers::Contains("150"));
    REQUIRE_THAT(res, Catch::Matchers::Contains("TestBand"));
}