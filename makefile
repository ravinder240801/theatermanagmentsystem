main: main.o Event.o Film.o Livemusic.o Standupcomedy.o
	g++ main.o Event.o Film.o Livemusic.o Standupcomedy.o -o main

test: test.o Event.o Film.o Livemusic.o Standupcomedy.o
	g++ test.o Event.o Film.o Livemusic.o Standupcomedy.o -o test

main.o: main.cpp
	g++ -c main.cpp

Event.o: Event.cpp Event.h
	g++ -c Event.cpp

Film.o: Event.o Film.cpp Film.h
	g++ -c Film.cpp

Livemusic.o: Event.o Livemusic.cpp Livemusic.h
	g++ -c Livemusic.cpp

Standupcomedy.o: Event.o Standupcomedy.cpp Standupcomedy.h
	g++ -c Standupcomedy.cpp

test.o: test.cpp
	g++ -c test.cpp

clean:
	del *.o main main.exe test test.exe
