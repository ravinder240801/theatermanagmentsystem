/*
    main.cpp
    Author: M00804377
    Created: 25 Jan,2022
    Updated: 25 Jan,2022
*/
#include <iostream>
#include <vector>
#include <fstream>
#include <limits>
#include <sstream>
#include "Film.h"
#include "LiveMusic.h"
#include "StandUpComedy.h"

using namespace std;

int get_int(string msg)
{
    int t;
    cout << msg;
    try
    {
        cin >> t;
        if ((int)t != t || t <= 0)
        {
            cout << "Enter a positive number!" << endl;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            return get_int(msg);
        }
        return t;
    }
    catch (...)
    {
        cout << "Enter a valid integer" << endl;
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        return get_int(msg);
    }
}

void show_films(vector<Film> films)
{
    cout << "Films:" << endl;
    for (int i = 0; i < films.size(); i++)
    {
        cout << to_string(i + 1) + ". " + films[i].name << endl;
    }
}

void show_music_shows(vector<Livemusic> musics)
{
    cout << "Live Music Shows:" << endl;
    for (int i = 0; i < musics.size(); i++)
    {
        cout << to_string(i + 1) + ". " + musics[i].name << endl;
    }
}

void show_comedy_shows(vector<Standupcomedy> comdeyShows)
{
    cout << "Stand Up Comedy Shows:" << endl;
    for (int i = 0; i < comdeyShows.size(); i++)
    {
        cout << to_string(i + 1) + ". " + comdeyShows[i].name << endl;
    }
}

int main()
{
    vector<Film> films;
    vector<Livemusic> musics;
    vector<Standupcomedy> comdeyShows;
    int vaccantSeatings = 200;
    int VaccantStandings = 300;
    int choice = 0;
    while (choice != 7)
    {

        choice = get_int("\nSelect an option\n1. Add new Booking\n2. Cancel Booking\n3. List all Events\n4. List Details of an event\n5. Load data from file\n6. Save data to file\n7. Exit\n-->");
        switch (choice)
        {
        case 1:
        {
            int t;
            t = get_int("Select type of booking\n1. Live Music\n2. Stand Up Comedy\n3. Film\n-->");
            switch (t)
            {
            case 1:
            {
                // Create Live Music
                Livemusic liveMusic;
                liveMusic.set_basic_data();
                if (liveMusic.capacity > VaccantStandings)
                {
                    cout << "Required number of seats are not vaccant!" << endl;
                    break;
                }
                liveMusic.set_music_data();
                VaccantStandings -= liveMusic.capacity;
                musics.push_back(liveMusic);
                cout << "Live Music show : " + liveMusic.name + " is booked successfully!" << endl;
                break;
            }
            case 2:
            {
                // Create Stand Up Comedy
                Standupcomedy standUpComedy;
                standUpComedy.set_basic_data();
                if (standUpComedy.capacity > vaccantSeatings)
                {
                    cout << "Required number of seats are not vaccant!" << endl;
                    break;
                }
                standUpComedy.set_standup_data();
                vaccantSeatings -= standUpComedy.capacity;
                comdeyShows.push_back(standUpComedy);
                cout << "Stand Up Comedy show : " + standUpComedy.name + " is booked successfully!" << endl;
                break;
            }
            case 3:
            {
                // Create Film
                Film film;
                film.set_basic_data();
                if (film.capacity > vaccantSeatings)
                {
                    cout << "Required number of seats are not vaccant!" << endl;
                    break;
                }
                film.set_film_data();
                vaccantSeatings -= film.capacity;
                films.push_back(film);
                cout << "Film : " + film.name + " is booked successfully!" << endl;
                break;
            }
            default:
            {
                cout << "Invalid option!" << endl;
                break;
            }
            }
            break;
        }
        case 2:
        {
            int t;
            t = get_int("Select type of booking\n1. Live Music\n2. Stand Up Comedy\n3. Film\n-->");
            switch (t)
            {
            case 1:
            {
                show_music_shows(musics);
                int del;
                del = get_int("Enter number to cancel: ");
                if (del <= musics.size())
                {
                    VaccantStandings += musics[del - 1].capacity;
                    musics.erase(musics.begin() + (del - 1));
                    cout << "Success" << endl;
                }
                break;
            }
            case 2:
            {
                show_comedy_shows(comdeyShows);
                int del;
                del = get_int("Enter number to cancel: ");
                if (del <= comdeyShows.size())
                {
                    vaccantSeatings += comdeyShows[del - 1].capacity;
                    comdeyShows.erase(comdeyShows.begin() + (del - 1));
                    cout << "Success" << endl;
                }
                break;
            }
            case 3:
            {
                show_films(films);
                int del;
                del = get_int("Enter number to cancel: ");
                if (del <= films.size())
                {
                    vaccantSeatings += films[del - 1].capacity;
                    films.erase(films.begin() + (del - 1));
                    cout << "Success" << endl;
                }
                break;
            }
            default:
            {
                cout << "Invalid option!" << endl;
                break;
            }
            }
            break;
        }
        case 3:
        {
            show_music_shows(musics);
            show_comedy_shows(comdeyShows);
            show_films(films);
            break;
        }
        case 4:
        {
            int t;
            t = get_int("Select type of booking\n1. Live Music\n2. Stand Up Comedy\n3. Film\n-->");
            switch (t)
            {
            case 1:
            {
                show_music_shows(musics);
                int sel;
                sel = get_int("Enter number to see details: ");
                if (sel <= musics.size())
                {
                    cout << musics[sel-1].get_details_string() << endl;
                }
                else
                {
                    cout << "This number do not exist!" << endl;
                }
                break;
            }
            case 2:
            {
                show_comedy_shows(comdeyShows);
                int sel;
                sel = get_int("Enter number to see details: ");
                if (sel <= musics.size())
                {
                    cout << comdeyShows[sel-1].get_details_string() << endl;
                }
                else
                {
                    cout << "This number do not exist!" << endl;
                }
                break;
            }
            case 3:
            {
                show_films(films);
                int sel;
                sel = get_int("Enter number to see details: ");
                if (sel <= films.size())
                {
                    cout << films[sel-1].get_details_string() << endl;
                }
                else
                {
                    cout << "This number do not exist!" << endl;
                }
                break;
            }
            default:
            {
                cout << "Invalid option!" << endl;
                break;
            }
            }
            break;
        }
        case 5:
        {
            // load from file
            films.clear();
            musics.clear();
            comdeyShows.clear();
            char delim, tp;
            string data;
            string l;
            ifstream f("BookingData.txt");
            if (f.is_open())
            {
                while (getline(f, l))
                {
                    stringstream ss(l);
                    ss >> tp;
                    ss >> delim;
                    if (tp == 'f')
                    {
                        Film obj;
                        getline(ss, data, delim);
                        obj.name = data;

                        getline(ss, data, delim);
                        obj.capacity = stoi(data);

                        getline(ss, data, delim);
                        obj.hero = data;

                        getline(ss, data, delim);
                        obj.dimension = data;

                        films.push_back(obj);
                    }
                    else if (tp == 'm')
                    {
                        Livemusic obj;
                        getline(ss, data, delim);
                        obj.name = data;

                        getline(ss, data, delim);
                        obj.capacity = stoi(data);

                        getline(ss, data, delim);
                        obj.singer = data;

                        getline(ss, data, delim);
                        obj.band = data;

                        musics.push_back(obj);
                    }
                    else if (tp == 'c')
                    {
                        Standupcomedy obj;
                        getline(ss, data, delim);
                        obj.name = data;

                        getline(ss, data, delim);
                        obj.capacity = stoi(data);

                        getline(ss, data, delim);
                        obj.comedian = data;

                        comdeyShows.push_back(obj);
                    }
                }
                f.close();
            }
            break;
        }
        case 6:
        {
            // save to file
            ofstream f("BookingData.txt");
            if (f.is_open())
            {
                for (int i = 0; i < films.size(); i++)
                {
                    f << films[i].get_file_string();
                }
                for (int i = 0; i < musics.size(); i++)
                {
                    f << musics[i].get_file_string();
                }
                for (int i = 0; i < comdeyShows.size(); i++)
                {
                    f << comdeyShows[i].get_file_string();
                }
                f.close();
            }
            break;
        }
        case 7:
        {
            cout << "Bye" << endl;
            break;
        }
        default:
        {
            cout << "Invalid option!" << endl;
            break;
        }
        }
    }
    return 0;
}